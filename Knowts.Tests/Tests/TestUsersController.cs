﻿using System.Web.Http;
using System.Web.Http.Results;
using Knowts.Business.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Knowts.WebApi.Controllers;

namespace Knowts.Tests.Tests
{
    [TestClass]
    public class TestUsersController
    {
        [TestMethod]
        public void GetUserIdFound()
        {
            const string testUserId = "pawel.janikowski@gmail.com";
            var controller = new UsersController();
            IHttpActionResult actionResult = controller.Get(testUserId);
            var contentResult = actionResult as OkNegotiatedContentResult<UserDto>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(testUserId, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetUserIdNotFound()
        {
            var controller = new UsersController();

            IHttpActionResult actionResult = controller.Get("5");

            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void PostUserId()
        {
            const string testUserId = "testpost2@gmail.com"; 
            var testUserDto = new UserDto
            {
                Id = testUserId
            };
            var controller = new UsersController();

            IHttpActionResult actionResult = controller.Post(testUserDto);

            var resultUserDto = controller.Get(testUserId);
            var contentResult = resultUserDto as OkNegotiatedContentResult<UserDto>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(testUserId, contentResult.Content.Id);
        }

    }
}
