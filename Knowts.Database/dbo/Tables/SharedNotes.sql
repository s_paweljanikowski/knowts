﻿CREATE TABLE [dbo].[SharedNotes] (
    [NoteId] INT            NOT NULL,
    [UserId] NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_SharedNotes] PRIMARY KEY CLUSTERED ([NoteId] ASC, [UserId] ASC),
    CONSTRAINT [FK_SharedNotes_Notes] FOREIGN KEY ([NoteId]) REFERENCES [dbo].[Notes] ([Id]),
    CONSTRAINT [FK_SharedNotes_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

