﻿CREATE TABLE [dbo].[Tasks] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [TaskText] NVARCHAR (MAX) NULL,
    [NoteId]   INT            NOT NULL,
    [IsDone]   BIT            CONSTRAINT [DF_Tasks_isDone] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Tasks_Notes] FOREIGN KEY ([NoteId]) REFERENCES [dbo].[Notes] ([Id])
);



