﻿CREATE TABLE [dbo].[Notes] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Title]            NVARCHAR (250) NULL,
    [Text]             NVARCHAR (MAX) NULL,
    [ModificationDate] DATETIME2 (7)  CONSTRAINT [DF_Notes_ModificationDate] DEFAULT (getdate()) NOT NULL,
    [UserId]           NVARCHAR (250) NOT NULL,
    [IsDeleted]        BIT            CONSTRAINT [DF_Notes_isDeleted] DEFAULT ((0)) NOT NULL,
    [ImageUrl]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Notes] PRIMARY KEY CLUSTERED ([Id] ASC)
);











