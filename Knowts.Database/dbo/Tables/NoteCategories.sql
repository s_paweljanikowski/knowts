﻿CREATE TABLE [dbo].[NoteCategories] (
    [NoteId]     INT NOT NULL,
    [CategoryId] INT NOT NULL,
    CONSTRAINT [PK_NoteCategories] PRIMARY KEY CLUSTERED ([NoteId] ASC, [CategoryId] ASC),
    CONSTRAINT [FK_NoteCategories_Categories] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Categories] ([Id]),
    CONSTRAINT [FK_NoteCategories_Notes] FOREIGN KEY ([NoteId]) REFERENCES [dbo].[Notes] ([Id])
);

