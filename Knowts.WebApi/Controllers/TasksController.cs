﻿using System.Web.Http;
using Knowts.Business.Dtos;
using Knowts.Business.Managers;

namespace Knowts.WebApi.Controllers
{
    public class TasksController : ApiController
    {
        private readonly TasksManager _tasksManager = new TasksManager();

        public IHttpActionResult Post(TaskDto taskDto)
        {
            var isAdded = _tasksManager.AddTask(taskDto);
            if (isAdded)
                return Ok();
            return BadRequest();
        }

        public IHttpActionResult Put(TaskDto taskDto)
        {
            var isUpdated = _tasksManager.UpdateTask(taskDto);
            if (isUpdated)
                return Ok();
            return BadRequest();
        }

        public IHttpActionResult Delete(int id)
        {
            var isDeleted = _tasksManager.DeleteTask(id);
            if (isDeleted)
                return Ok();
            return BadRequest();
        }
    }
}
