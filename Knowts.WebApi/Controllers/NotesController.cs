﻿using Knowts.Business.Managers;
using System.Web.Http;
using Knowts.Business.Dtos;

namespace Knowts.WebApi.Controllers
{
    public class NotesController : ApiController
    {
        private readonly NotesManager _notesManager = new NotesManager();

        public IHttpActionResult Get(int id)
        {
            var note = _notesManager.GetNote(id);
            if (note == null)
                return NotFound();
            return Ok(note);
        }

        public IHttpActionResult Post(NoteDto noteDto)
        {
            var isAdded = _notesManager.AddNote(noteDto);
            if (isAdded != null)
                return Ok(isAdded);
            return BadRequest();
        }

        public IHttpActionResult Put(NoteDto noteDto)
        {
            var isUpdated = _notesManager.UpdateNote(noteDto);
            if (isUpdated)
                return Ok(noteDto.Id);
            return BadRequest();
        }

        public IHttpActionResult Delete(int id)
        {
            var isDeleted = _notesManager.DeleteNote(id);
            if (isDeleted)
                return Ok(id);
            return BadRequest();
        }

        [HttpPost]
        public IHttpActionResult AddCategory(int id, int categoryId)
        {
            var isAdded = _notesManager.IsCategoryAdded(id, categoryId);
            if(isAdded)
                return Ok();
            return BadRequest();
        }

        [HttpDelete]
        public IHttpActionResult DeleteCategory(int id, int categoryId)
        {
            var isDeleted = _notesManager.IsCategoryDeleted(id, categoryId);
            if (isDeleted)
                return Ok();
            return BadRequest();
        }
    }
}
