﻿using Knowts.Business.Managers;
using System.Web.Http;
using Knowts.Business.Dtos;

namespace Knowts.WebApi.Controllers
{
    public class UsersController : ApiController
    {
        private readonly UsersManager _usersManager = new UsersManager();

        public IHttpActionResult Get(string id)
        {
            var user = _usersManager.GetUser(id);
            if (user == null)
                return NotFound();
            return Ok(user);
        }

        public IHttpActionResult Post(UserDto userDto)
        {
            var isAdded = _usersManager.AddUser(userDto);
            if (isAdded)
                return Ok();
            return BadRequest();
        }
    }
}
