﻿using System.Web.Http;
using Knowts.Business.Dtos;
using Knowts.Business.Managers;

namespace Knowts.WebApi.Controllers
{
    public class CategoriesController : ApiController
    {
        private readonly CategoriesManager _categoriesManager = new CategoriesManager();

        public IHttpActionResult Post(CategoryDto categoryDto)
        {
            var isAdded = _categoriesManager.AddCategory(categoryDto);
            if (isAdded != null)
                return Ok(isAdded);
            return BadRequest();
        }

        public IHttpActionResult Delete(int id)
        {
            var isDeleted = _categoriesManager.DeleteCategory(id);
            if (isDeleted)
                return Ok();
            return BadRequest();
        }
    }
}
