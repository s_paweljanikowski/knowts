﻿var currentId;

function Category(categoryId, categoryName) {
    var self = this;

    self.categoryId = ko.observable(categoryId);
    self.categoryName = ko.observable(categoryName);

    self.onDeleteCategory = function () {
        var category;
        for (var i = 0; i < app.categories().length; i++) {
            var c = app.categories()[i];
            if (c.categoryId() === self.categoryId())
                category = c;
        }
        $.ajax(baseUrl + "Categories/" + currentId, {
            type: "DELETE",
            success: function (data) {
                app.categories.remove(category);
                app.isCategoryDeleted(true);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                alert("ERROR!!!");
            }
        });
    }

    self.setCurrentId = function() {
        currentId = self.categoryId();
    }
}