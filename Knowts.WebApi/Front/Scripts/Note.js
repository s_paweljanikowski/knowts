﻿var actualId;

function _isImageUrlCorrect(url) {
    var re = /\.(jpg|png|gif|JPG|PNG|GIF)\b/;
    return re.test(url);
}

function Note(noteTitle, noteText, noteDate, noteId, userId, imageUrl) {
    var self = this;

    self.noteTitle = ko.observable(noteTitle);
    self.noteText = ko.observable(noteText);
    self.noteDate = ko.observable(noteDate);
    self.noteId = ko.observable(noteId);
    self.userId = ko.observable(userId);
    self.imageUrl = ko.observable(imageUrl);
    self.newImageUrl = ko.observable();
    
    var date = moment().format('YYYY-MM-DD HH:mm:ss');

    self.update = function (callback) {
        var data = JSON.stringify({
            Id: self.noteId(),
            UserId: self.userId(),
            Title: self.noteTitle(),
            Text: self.noteText(),
            ModificationDate: date,
            ImageUrl: self.imageUrl()
        });

        $.ajax({
            url: baseUrl + "Notes/",
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            data: data,
            success: function (results) {
                if (callback)
                    callback();
            }
        });
    };

    self.deleteNote = function (note) {
        $.ajax(baseUrl + "Notes/" + actualId, {
            type: "DELETE",
            dataType: 'json',
            success: function (data) {
                app.notes.remove(function (note) {return note.noteId() == data });
            }
        });
    };

    self.onAddImage = function () {
       if (_isImageUrlCorrect(self.newImageUrl())) {
           self.imageUrl(self.newImageUrl());
           self.update(function() {
               self.newImageUrl('');
           });
       } else {
           alert("Wrong URL address");
       }
    }

    self.onDeleteImage = function () {
        self.imageUrl('');
        self.update();
    }

    self.setActualId = function() {
        actualId = self.noteId();
    };
}