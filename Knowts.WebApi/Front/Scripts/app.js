﻿var baseUrl = "http://localhost:12804/Api/";

function _isEmailValidated(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function _hideRegisterModal() {
    $("#registerModal").modal("hide");
}

function _hideAddCategoryModal() {
    $("#addCategoryModal").modal("hide");
}

function _hideDeleteCategoryModal() {
    $("#deleteCategoryModal").modal("hide");
}

function AppViewModel() {
    var self = this;

    self.userEmail = ko.observable();
    self.newUserEmail = ko.observable();

    self.isLoginView = ko.observable(true);
    self.isLoginWarning = ko.observable(false);
    self.isVisibleNoteAddInput = ko.observable(false);
    self.isAddCategoryModalVisible = ko.observable(false);
    self.isRegisterWarning = ko.observable(false);
    self.isRegisterSuccess = ko.observable(false);
    self.isNotValidEmailWarning = ko.observable(false);
    self.isAgreeChecked = ko.observable(false);
    self.isRegisterClicked = ko.observable(false);
    self.isCategoryAdded = ko.observable(false);
    self.isCategoryDeleted = ko.observable(false);

    self.notes = ko.observableArray();
    self.categories = ko.observableArray();

    self.newNoteTitle = ko.observable();
    self.newNoteText = ko.observable();

    self.newCategoryName = ko.observable();

    self._clearWarnings = function () {
        self.isLoginWarning(false);
        self.isRegisterWarning(false);
        self.isRegisterSuccess(false);
        self.isNotValidEmailWarning(false);
        self.isAgreeChecked(false);
        self.isRegisterClicked(false);
        self.newUserEmail("");
        self.userEmail("");
    }

    self.onRegister = function () {
        self.isNotValidEmailWarning(false);
        self.isRegisterWarning(false);
        self.isRegisterClicked(true);
        var userId = self.newUserEmail();
        self.newUserEmail("");
        if (_isEmailValidated(userId)) {
            $.ajax(baseUrl + "Users/", {
                data: JSON.stringify ({
                    "Id": userId
                }),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function() {
                    self.isRegisterSuccess(true);
                },
                error: function () {
                    self.isRegisterWarning(true);
                    self.isRegisterClicked(false);
                }
            });
        } else {
            self.isNotValidEmailWarning(true);
            self.isRegisterClicked(false);
        }
    }

    self.onLogIn = function () {
        self.isNotValidEmailWarning(false);
        self.isLoginWarning(false);
        var userId = self.userEmail();
        if (_isEmailValidated(userId)) {
            $.getJSON(baseUrl + "Users/" + userId)
            .done(function (data) {
                self.isLoginView(false);
                $.map(data.Notes, function (item) {
                    self.notes.push(new Note(item.Title, item.Text, item.ModificationDateString, item.Id, item.UserId, item.ImageUrl));
                    return item.Title;
                });
                $.map(data.Categories, function(item) {
                    self.categories.push(new Category(item.Id, item.Name));
                    return item.Name;
                });
                autosize(document.querySelectorAll("textarea"));
            })
            .fail(function () {
                self.isLoginWarning(true);
            });
        } else {
            self.isNotValidEmailWarning(true);
        }
    }

    self.onAddNote = function () {
        var userId = self.userEmail();
        var noteTitle = self.newNoteTitle();
        var noteText = self.newNoteText();
        var date; // = moment().format('YYYY-MM-DD hh:mm:ss');
        var noteId;
        var imageUrl;

        $.ajax({
            url: baseUrl + "Notes/",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify ({
                Title: noteTitle,
                Text: noteText,
                UserId: userId
            }),
            success: function (data) {
                noteId = data.Id;
                date = data.ModificationDate;
                imageUrl = data.ImageUrl;
                dateString = moment(date).format("YYYY-MM-DD HH:mm:ss");
                self.notes.unshift(new Note(noteTitle, noteText, dateString, noteId, userId, imageUrl));
                autosize(document.querySelectorAll("textarea"));
            }
        });
        self.newNoteTitle(null);
        self.newNoteText(null);
        self.isVisibleNoteAddInput(false);
    };

    self.showHideNoteAddInput = function () {
        self.isVisibleNoteAddInput(!self.isVisibleNoteAddInput());
    };

    self.onAddCategory = function() {
        var categoryId;
        var userId = self.userEmail();
        var categoryName = self.newCategoryName();
        $.ajax(baseUrl + "Categories/", {
            data: JSON.stringify({
                Name: categoryName,
                UserId: userId
            }),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                categoryId = data.Id;
                self.categories.unshift(new Category(categoryId, categoryName));
            }
        });
        self.newCategoryName(null);
        self.isCategoryAdded(true);
    }

    self.hideCategoryWarning = function() {
        self.isCategoryAdded(false);
        self.isCategoryDeleted(false);
    }
}
var app = new AppViewModel();

ko.applyBindings(app);
autosize(document.querySelectorAll("textarea"));