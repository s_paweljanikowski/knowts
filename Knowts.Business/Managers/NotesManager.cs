﻿using System;
using System.Linq;
using System.Data.Entity;
using Knowts.Business.Dtos;
using Knowts.Data.Model;
using System.Collections.Generic;

namespace Knowts.Business.Managers
{
    public class NotesManager
    {
        private readonly KnowtsDBEntities _db = new KnowtsDBEntities();

        public NoteDto GetNote(int id)
        {
            var note = _db.Notes
                .Include(n => n.Tasks)
                .Include(c => c.Categories)
                .FirstOrDefault(n => n.Id == id);
            if (note == null)
                return null;
            return new NoteDto(note);
        }

        public NoteDto AddNote(NoteDto noteDto)
        {
            try
            {
                var tasks = new List<Tasks>();
                if (noteDto.Tasks != null)
                {
                    foreach (var taskDto in noteDto.Tasks)
                    {
                        if (taskDto != null)
                        {
                            var task = new Tasks
                            {
                                Id = taskDto.Id,
                                TaskText = taskDto.TaskText,
                                IsDone = taskDto.IsDone
                            };
                            tasks.Add(task);
                        }
                    }
                }
                var categories = new List<Categories>();
                if (noteDto.Categories != null)
                {
                    foreach (var categoryDto in noteDto.Categories)
                    {
                        if (categoryDto != null)
                        {
                            var category = new Categories
                            {
                                Id = categoryDto.Id,
                                Name = categoryDto.Name,
                                UserId = categoryDto.UserId
                            };
                            categories.Add(category);
                        }
                    }
                }
                var note = new Notes
                {
                    Id = noteDto.Id,
                    Title = noteDto.Title,
                    Text = noteDto.Text,
                    ModificationDate = System.DateTime.Now, //= noteDto.ModificationDate,
                    UserId = noteDto.UserId,
                    ImageUrl = noteDto.ImageUrl,
                    Tasks = tasks,
                    Categories = categories
                };
                _db.Notes.Add(note);
                _db.SaveChanges();
                return new NoteDto(note);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateNote(NoteDto noteDto)
        {
            try
            {
                var note = _db.Notes.Find(noteDto.Id);
                _db.Entry(note).CurrentValues.SetValues(noteDto);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteNote(int id)
        {
            try
            {
                var note = _db.Notes.Find(id);
                note.IsDeleted = true;
                _db.Entry(note).CurrentValues.SetValues(note);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsCategoryAdded(int id, int categoryId)
        {
            var note = _db.Notes.Find(id);
            if (note == null)
                return false;
            var category = _db.Categories.Find(categoryId);
            if (category == null)
                return false;
            note.Categories.Add(category);
            _db.SaveChanges();
            return true;
        }

        public bool IsCategoryDeleted(int id, int categoryId)
        {
            var note = _db.Notes.Find(id);
            if (note == null)
                return false;
            var category = _db.Categories.Find(categoryId);
            if (category == null)
                return false;
            note.Categories.Remove(category);
            _db.SaveChanges();
            return true;
        }
    }
}