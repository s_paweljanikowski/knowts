﻿using System;
using System.Data.Entity;
using System.Linq;
using Knowts.Business.Dtos;
using Knowts.Data.Model;

namespace Knowts.Business.Managers
{
    public class CategoriesManager
    {
        private readonly KnowtsDBEntities _db = new KnowtsDBEntities();

        public CategoryDto AddCategory(CategoryDto catregoryDto)
        {
            try
            {
                var category = new Categories
                {
                    Id = catregoryDto.Id,
                    Name = catregoryDto.Name,
                    UserId = catregoryDto.UserId
                };
                _db.Categories.Add(category);
                _db.SaveChanges();
                return new CategoryDto(category);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool DeleteCategory(int id)
        {
            try
            {
                var category = _db.Categories.FirstOrDefault(c => c.Id == id);
                if (category == null)
                    return false;
                _db.Entry(category).State = EntityState.Deleted;
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
