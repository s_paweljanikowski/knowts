﻿using System;
using System.Linq;
using System.Data.Entity;
using Knowts.Business.Dtos;
using Knowts.Data.Model;

namespace Knowts.Business.Managers
{
    public class UsersManager
    {
        private readonly KnowtsDBEntities _db = new KnowtsDBEntities();

        public UserDto GetUser(string id)
        {
            var user = _db.Users
                .Include(u => u.Notes)
                .Include(c => c.Categories)
                .FirstOrDefault(u => u.Id == id);
            if (user == null)
                return null;
            return new UserDto(user);
        }

        public bool AddUser(UserDto userDto)
        {
            try
            {
                var user = new Users
                {
                    Id = userDto.Id
                };
                _db.Users.Add(user);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}