﻿using System;
using System.Data.Entity;
using System.Linq;
using Knowts.Business.Dtos;
using Knowts.Data.Model;

namespace Knowts.Business.Managers
{
    public class TasksManager
    {
        private readonly KnowtsDBEntities _db = new KnowtsDBEntities();

        public bool AddTask(TaskDto taskDto)
        {
            try
            {
                var task = new Tasks
                {
                    Id = taskDto.Id,
                    TaskText = taskDto.TaskText
                };
                _db.Tasks.Add(task);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateTask(TaskDto taskDto)
        {
            try
            {
                var task = _db.Tasks.Find(taskDto.Id);
                _db.Entry(task).CurrentValues.SetValues(taskDto);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteTask(int id)
        {
            try
            {
                var task = _db.Tasks
                    .FirstOrDefault(t => t.Id == id);
                if (task == null)
                    return false;
                _db.Entry(task).State = EntityState.Deleted;
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
