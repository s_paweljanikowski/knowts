﻿using Knowts.Data.Model;

namespace Knowts.Business.Dtos
{
    public class CategoryDto
    {
        public int Id;
        public string Name;
        public string UserId;

        public CategoryDto()
        {
        }

        public CategoryDto(Categories category)
        {
            Id = category.Id;
            Name = category.Name;
            UserId = category.UserId;
        }
    }
}
