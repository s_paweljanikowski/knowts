﻿using Knowts.Data.Model;

namespace Knowts.Business.Dtos
{
    public class TaskDto
    {
        public int Id { get; set; }
        public string TaskText { get; set; }
        public bool IsDone { get; set; }

        public TaskDto()
        {
        }

        public TaskDto(Tasks task)
        {
            Id = task.Id;
            TaskText = task.TaskText;
            IsDone = task.IsDone;
        }
    }
}
