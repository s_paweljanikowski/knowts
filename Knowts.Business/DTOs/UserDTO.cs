﻿using Knowts.Data.Model;
using System.Collections.Generic;
using System.Linq;

namespace Knowts.Business.Dtos
{
    public class UserDto
    {
        public string Id { get; set; }
        public List<NoteDto> Notes { get; set; }
        public List<CategoryDto> Categories { get; set; }

        public UserDto()
        {
        }

        public UserDto(Users user)
        {
            Id = user.Id;
            Notes = user.Notes.Where(n => !n.IsDeleted).Select(n => new NoteDto(n)).ToList();
            Categories = user.Categories.Select(c => new CategoryDto(c)).ToList();
        }
    }
}