﻿using System.Collections.Generic;
using System.Linq;
using Knowts.Data.Model;

namespace Knowts.Business.Dtos
{
    public class NoteDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public System.DateTime ModificationDate { get; set; }
        public string ModificationDateString
        {
            get { return ModificationDate.ToString("yyyy-MM-dd hh:mm:ss"); }
        }
        public string UserId { get; set; }
        public string ImageUrl { get; set; }
        public List<TaskDto> Tasks { get; set; }
        public List<CategoryDto> Categories { get; set; }

        public NoteDto()
        {
        }

        public NoteDto(Notes note)
        {
            Id = note.Id;
            Title = note.Title;
            Text = note.Text;
            ModificationDate = note.ModificationDate;
            UserId = note.UserId;
            ImageUrl = note.ImageUrl;
            Tasks = note.Tasks.Select(t => new TaskDto(t)).ToList();
            Categories = note.Categories.Select(c => new CategoryDto(c)).ToList();
        }
    }
}